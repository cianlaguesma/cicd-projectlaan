module.exports = {
  siteMetadata: {
    siteUrl: "https://projectlaan.org/",
    title: "Project LAAN",
    author: `@whitecarabao`,
    description:
      "Poverty is a barrier to life-saving health care. Project LAAN fights so that all Filipinos can exercise their right to health without financial burden.",
  },
  plugins: [
    "gatsby-plugin-styled-components",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "Project LAAN",
        start_url: "/",
        background_color: "#fff",
        theme_color: "#f60",
        display: "browser",
        icon: "src/images/favicon.svg",
      },
    },
  ],
};
