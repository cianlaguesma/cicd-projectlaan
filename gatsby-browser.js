import "@fontsource/lora/700-italic.css";
import "@fontsource/karla/700.css";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);
